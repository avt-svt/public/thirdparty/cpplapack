The folder cpplapack-r198 contains cpplapack subversion r198: https://svn.code.sf.net/p/cpplapack/code/trunk

This lapack version has been tested for the following configurations:
	- Windows 7 using Microsoft Visual Studio 2017
	- Red Hat Linux 8.2.1  using gcc 8.2.1
